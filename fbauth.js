$(document).ready(function() {

  FB.getLoginStatus(function(response) {
    if (response.session) {
      $('a#fb-logout').show();
    } else {
      $('a#fb-login').show();
    }
  });

  $('a#fb-login').click(function(){
    FB.login(function(response) {
      $('a#fb-logout').show();
      $('a#fb-login').hide();      
    });
    return false;
  });


  $('a#fb-logout').click(function(){
    FB.logout(function(response) {
      $('a#fb-login').show();
      $('a#fb-logout').hide();      
    });
    return false;
  });
});