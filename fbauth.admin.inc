<?php


/**
 * @file
 * Admin callbacks for the Facebook Auth module.
 */

/**
 * Implementation of hook_admin_settings()
 */
function fbauth_admin_settings_form(&$form_state) {

  $description =  t('To use Facebook authentication, you need to register your website as a Facebook application by visiting: !link', 
      array('!link'=>l(t('Facebook Application Registration page'), 'http://www.facebook.com/developers/createapp.php')));

  $form['facebook_app_id'] = array(
    '#type' => 'textfield',
    '#title' => t('Facebook Application ID'),
    '#default_value' => variable_get('facebook_app_id', ''),
    '#required' => TRUE,
    '#description' => $description,
  );

  $form['facebook_app_secret'] = array(
    '#type' => 'textfield',
    '#title' => t('Facebook Application Secret'),
    '#default_value' => variable_get('facebook_app_secret', ''),
    '#required' => TRUE,
    '#description' => $description,    
  );

  //-- Not used at this point
  /**$form['facebook_api_key'] = array(
    '#type' => 'textfield',
    '#title' => t('Facebook API Key'),
    '#default_value' => variable_get('facebook_api_key', ''),
    '#required' => FALSE,
  );**/

  return system_settings_form($form);
}