<?php

class fbauth_fb {

  var $fb_app_id;
  var $fb_api_key;
  var $fb_app_secret;
  var $fb_cookie_name; 
  var $fb_user; 

  function __construct() {
  
    $this->fb_app_id      = variable_get('facebook_app_id', '');
    $this->fb_app_secret  = variable_get('facebook_app_secret', '');
    //$this->fb_api_key     = variable_get('facebook_api_key', '');    
    $this->fb_cookie_name = 'fbs_' . $this->fb_app_id; 
    
    if (empty($this->fb_app_id) || empty($this->fb_app_secret)) {
      fbauth_ok(FALSE);
      return;
    }
    else {
      fbauth_ok(TRUE);
    }
    
    $sett = (array) $this;

    fbauth_facebook_settings($sett);
    
    $fb_user = $this->get_fb_user();
    fbauth_fb_user($fb_user);
    
  }
  
  function get_fb_user() {
    $cookie = $this->get_fb_cookie();    
    $cookie_user = $this->parse_facebook_cookie($this->fb_app_id, $this->fb_app_secret);
    $url = 'https://graph.facebook.com/me?access_token=' .$cookie_user['access_token'];
    $response = drupal_http_request($url);
    if ($response->status_message == "OK") {
      $user = json_decode($response->data);
      $user->photo = "http://graph.facebook.com/$user->id/picture";
      return $user;
    }
    
    fbauth_ok(FALSE);
    return FALSE;    
  }
  
  function parse_facebook_cookie($app_id, $application_secret) {
    $args = array();
    parse_str(trim($_COOKIE['fbs_' . $app_id], '\\"'), $args);
    ksort($args);
    $payload = '';
    foreach ($args as $key => $value) {
      if ($key != 'sig') {
        $payload .= $key . '=' . $value;
      }
    }
    if (md5($payload . $application_secret) != $args['sig']) {
      return null;
    }
    return $args;
  }

  
  function get_fb_cookie() {
    if (isset($_COOKIE[$this->fb_cookie_name])) {
      return $_COOKIE[$this->fb_cookie_name];
    }
    
    return FALSE;
  }


  /** @deprecated **/
  function get_javascript() {
    
    
    $javascript = <<<JS
          window.fbAsyncInit = function() {
            FB.init({appId: '$this->fb_app_id', status: true, cookie: true,
                     xfbml: true});
          };
          (function() {
            var e = document.createElement('script');
            e.type = 'text/javascript';
            e.src = document.location.protocol +
              '//connect.facebook.net/en_US/all.js';
            e.async = true;
            document.getElementById('fb-root').appendChild(e);
          }());        
JS;

    return $javascript;

  }
}